import plotly.graph_objects as go
from datetime import datetime, timedelta
from dateutil.tz import gettz as timezone

from seedftw.base.timeseries import timestep_start
from seedftw.environment.solar import (
    day_length,
    solar_altitude,
    solar_azimuth_angle,
    air_mass_ratio,
    declination,
    zenith_angle,
)

latitude = 45
longitude = 0
t = datetime.now().astimezone(timezone("UTC"))
day_length(t, latitude)

solar_altitude(t, latitude)

solar_azimuth_angle(t, longitude)


def iterate_on_latitudes(f, lats=[-70, -45, 0, 20, 45, 60, 70, 80], step="day"):
    t0 = datetime.now().astimezone(timezone("UTC"))
    if step == "day":
        dt = lambda i: timedelta(days=i)
        t0 = timestep_start("year", t0)
        n = 365
    elif step == "hour":
        dt = lambda i: timedelta(hours=i)
        t0 = timestep_start("day", t0)
        n = 24

    fig = go.Figure()
    for lat in lats:
        ts = []
        h_day = []
        for i in range(0, n):
            t_i = t0 + dt(i)
            if lat is None:
                h_day.append(f(t_i))
            else:
                h_day.append(f(t_i, lat))
            ts.append(t_i)

        fig.add_trace(go.Scatter(x=ts, y=h_day, name=(str(lat))))
        fig["layout"]["yaxis"]["title"] = f.__name__
    return fig


fig = iterate_on_latitudes(f=day_length, step="day")
fig.show("png")

fig = iterate_on_latitudes(f=air_mass_ratio, step="hour")
fig.show("png")

fig = iterate_on_latitudes(f=solar_altitude, step="hour")
fig.show("png")

fig = iterate_on_latitudes(f=zenith_angle, step="hour")
fig.show("png")

fig = iterate_on_latitudes(f=declination, step="day", lats=[None])
fig.show("png")

fig = iterate_on_latitudes(f=solar_azimuth_angle, step="hour")
fig.show("png")
