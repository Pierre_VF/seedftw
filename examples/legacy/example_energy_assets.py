# -*- coding: utf-8 -*-

# Make sure that Matplotlib is installed before running this sample

from seedftw.energy.energy_asset import wind_turbine_generator, solar_pv_generator
import plotly.express as px
import pandas as pd
import numpy as np


# Plotting the wind turbine
wind_speed = np.linspace(0, 50, num=501)
power_output = wind_turbine_generator(wind_speed)

df_wind = pd.DataFrame({"wind_speed": wind_speed, "power_output": power_output})

fig = px.line(
    df_wind,
    x="wind_speed",
    y="power_output",
    title="Example power curve for a wind turbine",
    labels=dict(wind_speed="Wind speed [m/s]", power_output="Power output [W]"),
)
fig.show()

# Plotting the solar PV
solar_radiation = np.linspace(0, 1200, num=1201)
power_output = solar_pv_generator(solar_radiation)

df_pv = pd.DataFrame({"solar_radiation": solar_radiation, "power_output": power_output})

fig2 = px.line(
    df_pv,
    x="solar_radiation",
    y="power_output",
    title="Example power curve for a solar PV setup",
    labels=dict(
        solar_radiation="Incident solar radiation [W/m^2]",
        power_output="Power output [W]",
    ),
)
fig2.show()


# Testing geographical coordinates
from seedftw.environment.geography import distance_between_coordinates

coordinates_aalborg = [57.0488, 9.9217]
coordinates_aarhus = [56.1629, 10.2039]
latitude = 57.0488
longitude = 9.9217

distance_between_coordinates(coordinates_aalborg, coordinates_aarhus)
distance_between_coordinates(coordinates_aarhus, coordinates_aalborg)


from seedftw.environment.denmark import (
    get_weather_station_data,
    get_weather_stations,
    get_closest_weather_station,
    get_data_for_coordinates,
)

df = get_weather_station_data()

ds = get_weather_stations()

df2 = get_weather_station_data(station=6072)

latitude = 57.0488
longitude = 9.9217
tylstrup = get_closest_weather_station(latitude, longitude)
df3 = get_weather_station_data(station=tylstrup["stationId"])


dfN = get_data_for_coordinates(latitude, longitude, resolution="raw")

# Testing battery optimisation

import numpy as np
import pandas as pd
from seedftw.energy.energy_asset import battery_optimal_controller

import matplotlib.pyplot as plt


# Battery characteristics
charge_efficiency = 0.98
discharge_efficiency = 0.98
capacity = 1e4
rated_power = 1e3


from seedftw.energy.denmark import electricity_average_co2_intensity

df = electricity_average_co2_intensity("DK1")
N = len(df)
price = df["CO2Emission"].values

N = 400
price = 1 + np.cos(0.25 * np.linspace(1, N, num=N, axis=0))


performance, timeseries = battery_optimal_controller(
    price, capacity, rated_power, charge_efficiency, discharge_efficiency
)

plt.subplot(3, 1, 1)
plt.plot(
    timeseries["t"],
    timeseries["output_power"],
    timeseries["t"],
    timeseries["input_power"],
)
plt.legend(["Pout", "Pin"])

plt.subplot(3, 1, 2)
plt.plot(timeseries["t"], timeseries["soc"][1:])
plt.legend(["SOC"])

plt.subplot(3, 1, 3)
plt.plot(timeseries["t"], timeseries["price"])
plt.legend(["price"])

print(performance)

print("Total revenue: " + str(np.round(performance["revenue"], 2)) + " €")
print("Total energy losses: " + str(np.round(performance["energy_losses"], 2)) + " kWh")


## -------------------------------------------------------
# Under development
from datetime import datetime, timedelta

T = get_weather_station_data(
    station="06031", resolution="day", start_date=(datetime.now() - timedelta(days=30))
)
T = get_weather_station_data(
    station=6031, resolution="day", start_date=(datetime.now() - timedelta(days=30))
)
T = get_weather_station_data(resolution="hour")

T_stations = get_weather_stations()

latitude = 57.0488
longitude = 9.9217
tylstrup = get_closest_weather_station(latitude, longitude)


# Getting a data
