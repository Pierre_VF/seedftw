# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 23:17:24 2020

@author: PVF
"""

import plotly.graph_objects as go
from plotly.subplots import make_subplots
import numpy as np
import pandas as pd

import seedftw.energy.denmark as spdk
from seedftw.energy.network import split_import_export
from seedftw.base.timeseries import timestep_start
from datetime import datetime, timedelta
from pytz import timezone

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from functools import lru_cache

# Loading data (with a cache)
@lru_cache(maxsize=2)
def load_data(t_now):
    area = "DK1"
    start = t_now - timedelta(hours=48)
    end = t_now + timedelta(hours=24)

    try:
        T_co2_past = spdk.electricity_average_co2_intensity(
            area, start=start, end=t_now
        )
    except:
        T_co2_past = []

    try:
        T_co2_upcoming = spdk.electricity_average_co2_intensity_forecast(
            area, start=t_now - timedelta(hours=2), end=end
        )
    except:
        T_co2_upcoming = []

    try:
        T_prod_past = spdk.electricity_production_and_exchange(
            area, start=start, end=t_now
        )
    except:
        T_prod_past = []

    data2plot = {
        "production": T_prod_past,
        "co2_past": T_co2_past,
        "co2_forecast": T_co2_upcoming,
    }

    return data2plot


def add_export_plot_part(
    fig, T_imports, T_exports, x, col_name, name, i_plot=2, j_plot=1
):

    if name == "Great Belt":
        color2use = "rgb(50, 200, 240)"
    elif name == "Germany":
        color2use = "rgb(75, 175, 60)"
    elif name == "Sweden":
        color2use = "rgb(10, 10, 10)"
    elif name == "Norway":
        color2use = "rgb(125, 125, 240)"
    elif name == "Netherlands":
        color2use = "rgb(150, 100, 100)"
    else:
        color2use = "rgb(175, 75, 200)"

    fig.add_trace(
        go.Scatter(
            x=x,
            y=-T_imports[col_name],
            name="Import " + name,
            mode="lines",
            legendgroup="ImportExport",
            line=dict(color=color2use),
            stackgroup="two",
        ),
        i_plot,
        j_plot,
    )
    fig.add_trace(
        go.Scatter(
            x=x,
            y=T_exports[col_name],
            name="Export " + name,
            mode="lines",
            legendgroup="ImportExport",
            line=dict(color=color2use),
            stackgroup="three",
        ),
        i_plot,
        j_plot,
    )


def plot(data2plot):

    T_prod_past = data2plot["production"]
    T_co2_past = data2plot["co2_past"]
    T_co2_upcoming = data2plot["co2_forecast"]

    local_tz = timezone("Europe/Copenhagen")
    try:
        x = T_prod_past.index.tz_convert(local_tz)
    except:
        x = T_prod_past.index

    # Create traces
    # fig = go.Figure()
    fig = make_subplots(3, 1)

    fig.add_trace(
        go.Scatter(
            x=x,
            y=T_prod_past["solar_generation"],
            name="Solar",
            mode="lines",
            legendgroup="Production",
            line=dict(width=0.5, color="rgb(131, 90, 241)"),
            stackgroup="one",  # define stack group
        ),
        1,
        1,
    )
    fig.add_trace(
        go.Scatter(
            x=x,
            y=T_prod_past["onshore_wind_generation"],
            name="Onshore wind",
            mode="lines",
            legendgroup="Production",
            line=dict(width=0.5, color="rgb(111, 231, 219)"),
            stackgroup="one",
        ),
        1,
        1,
    )
    fig.add_trace(
        go.Scatter(
            x=x,
            y=T_prod_past["offshore_wind_generation"],
            name="Offshore wind",
            mode="lines",
            legendgroup="Production",
            line=dict(width=0.5, color="rgb(184, 247, 212)"),
            stackgroup="one",
        ),
        1,
        1,
    )

    fig["layout"]["yaxis1"]["title_text"] = "Production [MWh/h]"

    # Separate imports and exports
    T_exports = T_prod_past[[]].copy()
    T_imports = T_prod_past[[]].copy()
    for col_i in T_prod_past:
        if col_i.startswith("import"):
            T_imports.loc[:, col_i], T_exports.loc[:, col_i] = split_import_export(
                T_prod_past.loc[:, col_i]
            )

    # Plot imports and exports
    plot_imp_exp = lambda col, name: add_export_plot_part(
        fig, T_imports, T_exports, x, col_name=col, name=name, i_plot=2, j_plot=1
    )
    plot_imp_exp("import_from_great_belt", "Great Belt")
    plot_imp_exp("import_from_germany", "Germany")
    plot_imp_exp("import_from_sweden", "Sweden")
    plot_imp_exp("import_from_norway", "Norway")
    plot_imp_exp("import_from_netherlands", "Netherlands")

    sum_of_exports = (
        -1
        * (
            T_prod_past[
                [
                    "import_from_great_belt",
                    "import_from_germany",
                    "import_from_sweden",
                    "import_from_norway",
                    "import_from_netherlands",
                ]
            ]
        ).sum(axis=1)
    )
    fig.add_trace(
        go.Scatter(
            x=x,
            y=sum_of_exports,
            mode="lines",
            line=dict(width=3, color="red"),
            name="Net export",
        ),
        2,
        1,
    )

    fig["layout"]["yaxis2"]["title_text"] = "Net export [MWh/h]"

    try:
        x_T_co2_past = T_co2_past.index.tz_convert(local_tz)
        fig.add_trace(
            go.Scatter(
                x=x_T_co2_past,
                y=T_co2_past.co2_intensity,
                mode="lines",
                legendgroup="CO2",
                name="historical CO2",
            ),
            3,
            1,
        )
    except:
        print("Error in historical CO2 plot")

    try:
        t_last_co2 = T_co2_past.index[-1]
        upcoming_co2_filtered = T_co2_upcoming[T_co2_upcoming.index > t_last_co2]
        T_co2_upcoming = upcoming_co2_filtered
    except:
        None

    try:
        x_T_co2_upcoming = T_co2_upcoming.index.tz_convert(local_tz)
        fig.add_trace(
            go.Scatter(
                x=x_T_co2_upcoming,
                y=T_co2_upcoming.co2_intensity,
                mode="lines",
                legendgroup="CO2",
                name="forecast of CO2",
            ),
            3,
            1,
        )
    except:
        print("Error in forecast CO2 plot")

    fig["layout"]["yaxis3"]["title_text"] = "Emission intensity [kgCO2eq/MWh]"
    fig.update_xaxes(title_text="Time")

    fig.update_layout(
        width=1400, height=1000, legend_traceorder="grouped", legend_tracegroupgap=40
    )
    fig.update_xaxes(matches="x")

    return fig


dash_app = dash.Dash(__name__, external_stylesheets=None)
app = dash_app.server
dash_app.title = "Power in DK1"

t_now = timestep_start("15min")

data2plot = load_data(t_now)
fig = plot(data2plot)

dash_app.layout = html.Div(
    children=[
        html.H2("Power status for DK1"),
        dcc.Graph(id="plot1", figure=fig),
        dcc.Interval(
            id="refresh_timer", interval=5 * 60 * 1000, n_intervals=0  # in milliseconds
        ),
    ]
)

# Adding auto update of plot
@dash_app.callback(Output("plot1", "figure"), Input("refresh_timer", "n_intervals"))
def auto_update_data(n):
    t_now = timestep_start("15min")
    data2plot = load_data(t_now)
    fig = plot(data2plot)
    return fig


if __name__ == "__main__":
    # dash_app.run(host='127.0.0.1', port=8080, debug=True)
    # dash_app.run_server(debug=True)
    dash_app.run_server(host="0.0.0.0", port=8080, debug=True)
