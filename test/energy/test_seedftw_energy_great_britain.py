from datetime import datetime, timedelta
import pandas as pd


def test_energy_co2_history():
    from seedftw.energy.great_britain import (
        get_emission_factors,
        electricity_average_co2_intensity,
        get_historical_generation,
        electricity_average_co2_intensity_forecast,
    )

    t1 = datetime.fromisoformat("2021-01-01 00:00:00+00:00")
    t2 = t1 + timedelta(hours=12)

    df0 = get_emission_factors()
    df1 = electricity_average_co2_intensity(
        area=None, start=t1, end=t2, resolution="hour"
    )
    df2 = electricity_average_co2_intensity_forecast(
        area=None, start=t1, end=t2, resolution="hour"
    )
    df3 = get_historical_generation(start=t1, end=t2, resolution="hour", area=None)

    df11 = electricity_average_co2_intensity(
        area="england", start=t1, end=t2, resolution="hour"
    )
    df12 = electricity_average_co2_intensity_forecast(
        area="england", start=t1, end=t2, resolution="hour"
    )
    df13 = get_historical_generation(
        area="england", start=t1, end=t2, resolution="hour"
    )

    assert isinstance(df0, dict)
    assert isinstance(df1, pd.DataFrame)
    assert isinstance(df11, pd.DataFrame)
    assert isinstance(df2, pd.DataFrame)
    assert isinstance(df12, pd.DataFrame)
    assert isinstance(df3, pd.DataFrame)
    assert isinstance(df13, pd.DataFrame)
