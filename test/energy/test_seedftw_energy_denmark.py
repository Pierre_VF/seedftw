from datetime import datetime, timedelta


def test_energy_co2_history():
    from seedftw.energy.denmark import (
        electricity_average_co2_intensity,
        electricity_average_co2_intensity_forecast,
        electricity_balance,
        electricity_distribution_average_co2_intensity,
        electricity_spot_price,
        electricity_production_and_exchange,
    )

    area = "DK1"
    t0 = datetime.fromisoformat("2021-01-01 00:00:00+00:00")
    t1 = t0 + timedelta(hours=12)

    df = electricity_average_co2_intensity("DK1")
    assert len(df) > 0, "Not getting CO2 data in"

    df1 = electricity_average_co2_intensity(area, t0, t1)
    df2 = electricity_average_co2_intensity_forecast(area, t0, t1)
    df3 = electricity_balance(area, t0, t1)
    df4 = electricity_spot_price(area, t0, t1)
    df5 = electricity_production_and_exchange(area, t0, t1)

    # TODO : fix this one
    # df6 = electricity_distribution_average_co2_intensity(t0, t1)
