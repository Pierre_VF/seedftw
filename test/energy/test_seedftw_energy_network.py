import pandas as pd


def test_exports():
    from seedftw.energy.network import split_import_export

    df_ie = pd.Series([1, -1, 2, 3])

    df_i, df_e = split_import_export(df_ie)
    assert (df_i >= 0).all()
    assert (df_e >= 0).all()

    df_i2, df_e2 = split_import_export(None)
    assert df_i2 is None
    assert df_e2 is None
