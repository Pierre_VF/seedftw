from seedftw.base.timeseries import datetime


def test_angles_data():

    from seedftw.environment.solar import (
        air_mass_ratio,
        hour_angle,
        declination,
        day_length,
        solar_altitude,
        solar_azimuth_angle,
        zenith_angle,
    )

    t = datetime.fromisoformat("2020-07-01 13:00:00+00:00")
    latitude = 57.046707
    longitude = 9.935932

    assert hour_angle(t) > 0
    assert declination(t) > 0
    assert day_length(t, latitude) > 0
    assert solar_azimuth_angle(t, longitude) > 0
    assert zenith_angle(t, latitude) > 0
    assert solar_altitude(t, latitude) > 0
    assert air_mass_ratio(t, latitude) > 0
