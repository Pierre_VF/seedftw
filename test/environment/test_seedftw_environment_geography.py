from seedftw.base.timeseries import datetime


def test_angles_data():

    from seedftw.environment.geography import distance_between_coordinates

    coordinates_1 = [57.046707, 9.935932]
    coordinates_2 = [57.046707, 9.935932]

    assert distance_between_coordinates(coordinates_1, coordinates_1) == 0
