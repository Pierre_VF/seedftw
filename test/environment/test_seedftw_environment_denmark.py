import pandas as pd

from seedftw import load_configuration
from seedftw.base.timeseries import now, timedelta, datetime

load_configuration()


def test_weather_data():

    import seedftw.environment.denmark as sedk

    t_min = now() - timedelta(days=10 * 365)
    __latest_version = "v2"
    T_dk = sedk.get_weather_stations(
        only_available=False, version=__latest_version, valid_since=None
    )
    T_dk0 = sedk.get_weather_stations(
        only_available=True, version=__latest_version, valid_since=None
    )
    T_dk1 = sedk.get_weather_stations(
        only_available=True, version=__latest_version, valid_since=t_min
    )

    # Get a list of the weather stations in the country
    df_weather_stations = sedk.get_weather_stations()
    assert isinstance(df_weather_stations, pd.DataFrame)

    latitude = 57.046707
    longitude = 9.935932
    closest = sedk.get_closest_weather_station(latitude, longitude)
    print(closest)

    # Remaining:
    # 1) select only valid stations
    # 2) select measurements data

    # Load data for a given weather station over a period

    latitude = 57.046707
    longitude = 9.935932
    start_date = datetime.now() - timedelta(days=3)
    end_date = datetime.now()
    resolution = "hour"  # or "raw","hour","day"
    parameters = [
        "ambient_temperature",
        "wind_speed",
        "wind_direction",
        "relative_humidity",
        "global_solar_radiation",
    ]  # you can also choose a subset of these

    # T_local_dmi_period = sedk.get_data_for_coordinates(latitude,longitude,start=start_date,end=end_date,
    #    resolution=resolution,parameters=parameters,version="v1")

    T_local_dmi_period = sedk.get_data_for_coordinates(
        latitude,
        longitude,
        start=start_date,
        end=end_date,
        resolution=resolution,
        parameters=parameters,
        version="v2",
    )

    assert isinstance(T_local_dmi_period, pd.DataFrame)
