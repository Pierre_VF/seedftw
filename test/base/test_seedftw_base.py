from datetime import datetime

import numpy as np
import pandas as pd


def test_geography():
    # Testing geographical coordinates
    from seedftw.environment.geography import distance_between_coordinates

    coordinates_aalborg = [57.0488, 9.9217]
    coordinates_aarhus = [56.1629, 10.2039]
    latitude = 57.0488
    longitude = 9.9217
    assert (
        distance_between_coordinates(coordinates_aalborg, coordinates_aarhus) < 120
    ), "Distance calculation is bad (too high)"
    assert (
        distance_between_coordinates(coordinates_aarhus, coordinates_aalborg) > 80
    ), "Distance calculation is bad (too small)"


def test_timeseries():
    from seedftw.base.timeseries import (
        now,
        microepoch_to_datetime_index,
        datetime_to_microepoch,
    )

    for step in [
        "raw",
        "1s",
        "15s",
        "30s",
        "1min",
        "5min",
        "15min",
        "30min",
        "hour",
        "day",
        "month",
        "year",
    ]:
        assert isinstance(now(step=step), datetime)

    t0 = now()
    mepoch0 = datetime_to_microepoch(t0)
    T = pd.DataFrame(index=[t0])
    T["microepoch"] = mepoch0
    assert np.isscalar(mepoch0), "datetime_to_microepoch does not return an int"
    assert (
        microepoch_to_datetime_index(T["microepoch"]) is not None
    ), "datetime_to_microepoch work"
    assert (
        microepoch_to_datetime_index(T["microepoch"]) is not None
    ), "datetime_to_microepoch work"
