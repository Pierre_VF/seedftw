from seedftw.exceptions import MovedToPlotneat


def group_legend_by_name(*args, **kwargs):
    raise MovedToPlotneat()


def minimalistic_show(*args, **kwargs):
    raise MovedToPlotneat()


def clean_legend(*args, **kwargs):
    raise MovedToPlotneat()
